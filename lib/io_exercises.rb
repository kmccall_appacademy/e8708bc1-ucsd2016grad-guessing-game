# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game
  number = 1 + rand(100)
  count = 0
  found = false
  while found == false
    puts "guess a number"
    input = gets.to_i
    puts input
    count += 1
    if input < number
      puts "too low"
    elsif input > number
      puts "too high"
    else
      puts count
      found = true
    end
  end
end


# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
